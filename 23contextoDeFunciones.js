function Persona(nombre, apellido, altura) {
  this.nombre = nombre
  this.apellido = apellido
  this.altura = altura
  this.nacionalidad = 'Mexicano'
}

const ESTATURA_PROMEDIO = 1.75

Persona.prototype.saludarArrow = () => {
  console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
}
// ¿Que sucede con las arrow functions? Dentro de ellas, "this" se refiere al "this" global. En la consola, "this" tendra por valor window.
console.log("this === window");
console.log(this === window);

Persona.prototype.soyAltoArrow = () => {
  this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
}

Persona.prototype.saludar = function () {
  console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
}
Persona.prototype.soyAlto = function () {
  this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
} // Por ello es necesario escribir las funciones "completas" si las usaremos en un prototipo, pues estas usan "this" del constructor.

var luis = new Persona('Luis', 'Carrillo', 1.62)
var greta = new Persona('Greta', 'Tena', 1.59)
var axel = new Persona('Axel', 'Godinez', 1.76)
