var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32,
  peso: 69,
}
// "for" nos permite realizar una tarea de manera repetitiva, es decir, un loop.
// En este ejemplo haremos una simulacion del cambio de peso de una persona en el transcurso de un año.
console.log(`Al inicio del año ${luis.nombre} pesa ${luis.peso} kg`);

const CAMBIO_DE_PESO = 0.2 // Digamos que cada vez que Luis sube o baja de peso lo hace a razon de 200 grs.
const DIAS_POR_ANO = 365

 // Establecemos arrow functions para las posibilidades de que aumente o disminuya el "peso" del objeto cada cierto dia del año.
const aumentarDePeso = (persona, incremento) => persona.peso += CAMBIO_DE_PESO // Al usar dos parametros es necesario el uso de los parentesis
const adelgazar = (persona, incremento) => persona.peso -= CAMBIO_DE_PESO

 // "for" utiliza tres argumentos para saber cuantos veces debe repetirse: en el primero, se establece a partir de que valor comienza, el segundo indica cuantas veces debe repetirse (al usar menor que, estamos indicando que se repetira hasta que i sea mayor a 365, es decir, el valor de dias por año, en dicho momento el resultado de esa comparacion sera false y dejara de repetirse) y el ultimo argumento es para indicar cuanto aumentara i en cada repeticion, con un doble mas el aumento es en 1 por cada ciclo.
for (var i = 1; i <= DIAS_POR_ANO; i++) {
  var random = Math.random() //Math.random nos arroja un numero aleatorio entre 0 y uno. Esta variable lograra que nuestra simulacion sea diferente en cada ejecución.
  if (random < 0.25) { // Cada que random sea menor a 0.25 significa que ese "dia" de simulacion no hubo actividad fisica y si mucha ingesta calórica.
    aumentarDePeso(luis)
} else if (random < 0.5) { // Si random tiene un valor entre 0.25 y 0.50 ese dia la ingesta calórica fue moderada y hubo suficiente actividad fisica.
    adelgazar(luis)
  }
} // Los "dias" que random sea mayor a 0.5 no habra cambios en el peso, por eso no hacemos un condicional con esos valores.
console.log(`Al final del año ${luis.nombre} pesa ${luis.peso.toFixed(1)} kg`);
// Recordemos que "for" se ejecutara solo, en el momento en que el codigo llegue al punto donde fue declarado. Es por esa razon que los atributos solo se modificaron llegados a este log, y en el primer log muestra los atributos originales.

//Ahora usemos una version mas corta ocupando el condicional ternario y llamando funciones por medio de variables:
var otro = {
    nombre: 'Carlos',
    apellido: 'Arenas',
    edad: 28,
    peso: 76.2
};
const AJUSTE_PESO = .2, DIAS_DEL_ANO = 365;

// En una sola funcion tenemos aumentarDePeso y adelgazar, manejando dos niveles ternarios. En el primer nivel "+= AJUSTE_PESO" da el aumento, se declara el else con ":" y se abre una nueva condicion, el "segundo nivel", ahi "-= AJUSTE_PESO" lo "adelgaza". El ultimo ":" se declara vacio (0).
var nuevoPeso = random => random <= .25 ? otro.peso += AJUSTE_PESO : random <= .5 ? otro.peso -= AJUSTE_PESO : 0;

console.log(`Al inicio del año ${otro.nombre} pesa ${otro.peso} Kg.`);

for(let i = 1; i <= DIAS_DEL_ANO; i++){
    var random = Math.random();
    nuevoPeso(random); // Las condicionales ya estan dentro del arrow function, lo cual limpia nuestro codigo.
}

console.log(`Al final del año ${otro.nombre} pesa ${otro.peso.toFixed(1)} Kg.`);
