// Javascript tiene un "constructor", el cual sirve para crear objetos a partir de un "prototipo"

function Persona(nombre, apellido, altura) {
  this.nombre = nombre // "this" establece un atributo y los retorna a los objetos que creemos, y los podemos ligar a los parametros que requiere el prototipo.
  this.apellido = apellido
  this.altura = altura
  this.nacionalidad = 'Mexicano'  // Si en lugar de hacer referencia a uno de los parametros declaramos un valor, todos los objetos creados con el prototipo tendran el mismo atributo.
}

const ESTATURA_PROMEDIO = 1.75
// Es posible añadir funciones a los prototipos como un "atributo" extra, basta con declararlos de la siguiente manera, para llamarlos usaremos la sintaxis de funcion e.g. luis.saludar()
Persona.prototype.saludar = function () {
  console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
}
Persona.prototype.soyAlto = function () {
  this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
}

var luis = new Persona('Luis', 'Carrillo', 1.62) // "new" crea un nuevo objeto con el prototipo que le indiquemos (Persona).
var greta = new Persona('Greta', 'Tena', 1.59) //Los argumentos que escribamos tomaran su lugar como atributos del nuevo objeto exactamente en el orden que establecimos en el prototipo.
var axel = new Persona('Axel', 'Godinez', 1.76)

console.log("greta.saludar()");
console.log(greta.saludar()); // y asi es como llamas a una funcion dentro de un objeto, el mismo metodo que ya hemos ocupado para las funciones inherentes a JavaScript.
