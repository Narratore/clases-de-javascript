// Los arrays son estructuras que nos permiten organizar elementos dentro de una colección. Estos elementos pueden ser números, strings, booleanos, objetos, etc.
var luis = {
  nombre:'Luis',
  apellido:'Carrillo',
  edad:32,
}
var axel ={
  nombre:'Axel',
  apellido:'Godinez',
  edad:35,
}
var jorge={
  nombre:'Jorge',
  apellido:'Medina',
  edad:36,
}
var gonzalo={
  nombre:'Gonzalo',
  apellido:'Pardo',
  edad:36,
}
var jassiel={
  nombre:'Jassiel',
  apellido:'Reyes',
  edad:35,
}
var greta ={
  nombre:'Greta',
  apellido:'Tena',
  edad:29
}

var personas = [luis, jorge, axel, gonzalo, jassiel, greta] // Para indicar que es un array, usamos los corchetes "[]" y dentro de ellos los elementos de nuestro array separados por comas.
// En un array cada objeto tendra un valor posicional, empezando por el 0 (es decir, en este ejemplo la posicion 0 es luis, la 3 es gonzalo, etc.)

//Para llamar a cualquier atributo de los objetos del array poniendo su posicion entre cochetes y despues el atributo deseado, e.g.
console.log("personas[5].apellido");
console.log(personas[5].apellido);

// Podemos filtrar cierta informacion de nuestro array por medio de una estructura "for".
for (var i = 0; i < personas.length; i++) { //el ciclo for empezara desde la primer posicion (i=0) y hasta el ultimo de los objetos.
  var persona = personas[i] // usara la variable "persona" para devolver cada elemento del array. Es decir, por cada iteracion de for tendremos persona[0], persona[1], etc.
  console.log(`${persona.nombre} tiene ${persona.edad} años.`);
}
