//JavaScript consigue datos externos (es decir, llama datos de otros servidores) a traves de las APIs, para lo cual usaremos jQuery, el cual proporciona los protocolos necesarios para esta operacion.

// Es buena practica establecer constantes para las direcciones de los recursos que necesitemos.
const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id' // Para el presente ejercicio nos valdremos de la API de Star Wars (SWAPI). Esta contiene datos sobre todas las peliculas y personajes de la saga.

const lukeUrl= `${API_URL}${PEOPLE_URL.replace(':id', 1)}` // El metodo "replace" sirve para modificar cualquier string. Cambia la cadena de texto especificada en el primer parametro por la del segundo parametro
const opts= { crossDomain:true } // para definir las opciones del request
const onPeopleResponse= function(character) { // En la funcion callback, solo establecemos un parametro el cual es el objeto que se consiguio del request.
    console.log(arguments); // usando el metodo arguments podemos visualizar las propiedades del objeto recibido.
    console.log(`Hola, yo soy ${character.name}`)
}
$.get(lukeUrl, opts, onPeopleResponse) // $.get es una funcion de jquery que realiza un request. Necesita algunos parametros: primero la url; segundo opciones para indicarle a jquery que el request es hacia un recurso externo (opts); al final, se asigna un callback (Es decir, una funcion en respuesta al objeto recibido).
