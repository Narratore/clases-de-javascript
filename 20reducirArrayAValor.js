var luis = {
  nombre:'Luis',
  apellido:'Carrillo',
  edad:32,
  altura: 1.62,
  cantidadDeLibros: 115
}
var axel ={
  nombre:'Axel',
  apellido:'Godinez',
  edad:35,
  altura: 1.74,
  cantidadDeLibros: 54
}
var jorge={
  nombre:'Jorge',
  apellido:'Medina',
  edad:36,
  altura: 1.76,
  cantidadDeLibros: 266
}
var gonzalo={
  nombre:'Gonzalo',
  apellido:'Pardo',
  edad:36,
  altura: 1.81,
  cantidadDeLibros: 85
}
var jassiel={
  nombre:'Jassiel',
  apellido:'Reyes',
  edad:35,
  altura: 1.78,
  cantidadDeLibros: 230
}
var greta ={
  nombre:'Greta',
  apellido:'Tena',
  edad:29,
  altura: 1.59,
  cantidadDeLibros: 321
}

var personas = [luis, jorge, jassiel, gonzalo, axel, greta]

// Para obtener la suma de algun atributo de todos los elementos de nuestro array, podriamos escribir una estructura for:
// var acum = 0
//
// for (var i = 0; i < personas.length; i++) {
//   acum = acum + personas[i].cantidadDeLibros
// }

// Tambien existe el metodo "reduce", el cual funciona de una manera sencilla

// Creamos una arrow function que haga la suma que esperamos:
const reducer = (acum, { cantidadDeLibros }) => acum + cantidadDeLibros // donde trabajamos con dos parametros, el acumulador y la clave de la cual necesiamos el total.

var totalDeLibros = personas.reduce(reducer, 0) // Los argumentos de "reduce" son: la funcion que hara la suma de los libros y el valor inicial de nuestro "acumulador". Como se puede observar, usa menos lineas y es mas "elegante".

console.log(`En total todos tienen ${totalDeLibros} libros`);
