var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32,
  ingeniero: false,
  cuentacuentos: true,
  cantante: false,
  bailarin: false,
  estudiante: true,
}
var alexis = {
  nombre: 'Alexis',
  edad: 7,
}

// Es una buena practica el mantener el codigo legible, y el meter toda la logica de nuestro programa en el cuerpo de una sola funcion puede volverla dificil de leer.

function imprimirSiEsMayorDeEdadImplicito(persona) {
  if (persona.edad >= 18) { // Podemos notar que tenemos un operador matematico del cual nosotros conocemos su razon de ser, pero si algun otro programador  ve el codigo, quiza no comprenda a que razón viene esta operacion.
    console.log(`Funcion implicita: ${persona.nombre} es mayor de edad`);
  } else {
    console.log(`Funcion implicita: ${persona.nombre} es menor de edad`);
  }
}

console.log(imprimirSiEsMayorDeEdadImplicito(luis));
console.log(imprimirSiEsMayorDeEdadImplicito(alexis));

// Para resolver esto, es recomendable descomponer nuestra funcion en otras mas pequeñas que den razón de lo que esta sucediendo
// Veamos como logramos usar una funcion mas humanamente legible para todo el que tenga acceso al codigo:

// Declaramos una constante para especificar cual es la razon de utilizar 18 como valor en nuestra funcion esMayorDeEdad, de esta manera no hay "magic numbers" que confundan a alguien mas que lea nuestro codigo (o a nosotros mismos si usamos valores inventados al momento y al encontrarlos en un posteriormente no recordamos su procedencia)

// Para no presentar "magic numbers", es decir, valores numericos sin ninguna explicación, los usamos dentro de constantes. Hay diversas razones para evitar los numeros magicos, en este ejemplo la razon es que no en todo el mundo se alcanza la mayoria de edad a los 18 años; en otros paises es 21, por lo cual a un programador externo no le resulte claro encontrarse con un 18.
const MAYORIA_DE_EDAD = 18 // Es convención nombrar a las constantes en mayusculas, con espacios "snake", es decir, separando las palabras con guiones bajos.
function esMayorDeEdad(persona) { // Esta funcion "hija" va a usar el mismo parametro que la funcion principal.
    return persona.edad >= MAYORIA_DE_EDAD // Aqui escribimos la logica que en la primer version de nuestra funcion se incluia en el cuerpo  (persona.edad >= 18), necesitamos que nos devuelva el valor, por ello ocupamos return.
}
function imprimirSiEsMayorDeEdad(persona) {
  if (esMayorDeEdad(persona)) { // En lugar de usar un operador matematico, nuestra condicion se valida por medio de una funcion que devuelve un valor boleano.
    console.log(`Funcion explicita: ${persona.nombre} es mayor de edad`);
  } else {
    console.log(`Funcion explicita: ${persona.nombre} es menor de edad`);
  }
}

console.log(imprimirSiEsMayorDeEdad(luis));
console.log(imprimirSiEsMayorDeEdad(alexis));
