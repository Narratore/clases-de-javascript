var nombre = 'Luis', apellido = 'Carrillo' // Puedes declarar multiples variables en una linea separandolos por una coma.

// Podemos usar una funcion en una variable para darle valor a una nueva. Es este ejemplo ocupamos la funcion toUpperCase() en la variable "nombre" y la funcion toLowerCase en la variable apellido, definidas anteriomente, colocando un punto entre la variable y la funcion deseada.
var nombreEnMayusculas = nombre.toUpperCase()
var apellidoEnMinusculas = apellido.toLowerCase()

//Algunas funciones se emplean dejanto el parametro vacio, algunas necesitan un valor para realizar su accion.
var primeraLetraDelNombre = nombre.charAt(0) //charAt nos dice que caracter se encuentra en la posicion del parametro, el primer caracter tiene un valor de 0, el sugundo caracter tendra un valor de 1 y asi sucesivamente.
var cantidadDeLetrasDelNombre = nombre.length //aunque length no es una funcion precisamente (es un atributo en realidad, por eso no lo requiere) la utilizamos de la misma manera, despues de un punto. La funcion nos devolvera el numero de caracteres en el valor de la variable.
var ultimaLetraDelNombre = nombre.charAt(nombre.length - 1) //aqui estamos anidando una funcion dentro de otra, para obtener la posicion del ultimo caracter del valor de la variable "nombre". Usamos la función length y le restamos uno para tomar en cuenta la posicion 0.
console.log('Tu nombre acaba con ' + ultimaLetraDelNombre)

// Interpolacion de texto
// Una manera mas simple de concatenar texto es por medio del uso de comillas invertidas (en el teclado latam se usa  AltGr + "}"), dentro de ellas podemos usar texto de forma natural, y las variables se solicitan colocandolas entre corchetes despues de un signo "$" es decir "${}"
var nombreCompleto = `Tu nombre es ${nombre} ${apellido}.` // podemos usar funciones con las variables, por ejemplo ${apellido.toUpperCase}
console.log(nombreCompleto)

var str = apellido.substr(1, 4) // Esta es la funcion substring, que nos dice los caracteres del valor de la variable desde la posicion del primer parametro y hasta la posicion del segundo parametro.
console.log('Resultado substring ' + str);

var ultimaLetraDeApellido = apellido.substr(-1) //Usando el parametro -1 con la funcion substr nos devuelve el ultimo caracter de la variable, es una altenativa mas elegante a apellido.charAt(nombre.length - 1)
console.log(`La ultima letra de tu apellido es ${ultimaLetraDeApellido}.`)
