var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32,
  ingeniero: false,
  cuentacuentos: true,
  cantante: false,
  bailarin: false,
  estudiante: true,
}
var alexis = {
  nombre: 'Alexis',
  edad: 7,
}
const MAYORIA_DE_EDAD = 18

function imprimirSiEsMayorDeEdad(persona) {
  if (esMayorDeEdad(persona)) {
    console.log(`${persona.nombre} es mayor de edad`);
  } else {
    console.log(`${persona.nombre} es menor de edad`);
  }
}
// Vamos a expresar la funcion esMayorDeEdad de una manera mas legible

// Javascript nos permite asignar una funcion a una variable, para esto debemos cambiar un poco el orden de la sintaxis:
var esMayorDeEdad1 = function(persona) { // Al leer el valor de la variable, nos encontramos con una funcion anonima.
  return persona.edad >= MAYORIA_DE_EDAD
} // Como esta variable tiene por valor una función, al momento de llamarla hay que darle un parametro, e.g. esMayorDeEdad1(axel). Prueba a usar esta variable como condicion en imprimirSiEsMayorDeEdad

// Para simplificar utilizaremos un "arrow function" el cual se compone por un signo de igual seguido de un 'mayor que' "=>", de ahi su nombre, "flecha". Al escribir este signo, ya no es necesario indicar que es una función. Las arrow functions usan como parametro lo que se encuentre a la izquierda de "=>"
const esMayorDeEdad2 = persona => { // Podemos obviar los parentesis del parametro cuando la funcion ocupa solo uno.
  return persona.edad >= MAYORIA_DE_EDAD
} // Intenta utilizar esMayorDeEdad2 en la funcion imprimirSiEsMayorDeEdad

// Si el unico cometido de la funcion es que nos devuelva un valor, no es necesario indicar return ni hacer uso de las llaves, esto reduce todo a una sola linea.
const esMayorDeEdad3 = persona => persona.edad >= MAYORIA_DE_EDAD // Modifica imprimirSiEsMayorDeEdad para usar esta funcion en el condicional.

// Para desestructurar un atributo si usaremos los parentesis, pero mejora aun mas la legibilidad.
const esMayorDeEdad = ({ edad }) => edad >= MAYORIA_DE_EDAD


console.log(imprimirSiEsMayorDeEdad(luis));
console.log(imprimirSiEsMayorDeEdad(alexis));

// Si necesitamos que la condicion sea negativa, basta con usar "!" antes de llamar a la condicion.
function permitirAcceso(persona) {
  if (!esMayorDeEdad(persona)) { // En esta linea la condicion es "No sea mayor de edad", por ello hicimos uso del "!" el cual nos pide que su valor sea "false" para ejecutarse.
    console.log('Acceso Denegado')
    }
  }
console.log(permitirAcceso(alexis));

// Vamos a escribir una función llamada "Es menor de edad", la cual simplemente devuelva la negacion de la llamada a esMayorDeEdad

const esMenorDeEdad = (persona) => !esMayorDeEdad(persona) ? console.log('Que tu eres niño') : console.log('Pasele');
// aqui el uso de if es por medio de un operador ternario, donde ? es para declara lo que ocurriria con la condicion y ":" es para el operador else, todo en una linea. Podemos verlo asi: un ternario es a un if lo que un arrow es a una función, una manera de expresar lo mismo en menos espacio.
