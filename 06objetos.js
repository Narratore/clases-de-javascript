// Creamos un objeto como si fuera una variable, pero en lugar de asignar un valor, abrimos llaves (teclado LATAM la tecla despues de la ñ).
var luis = {
// ahora, declaramos los atributos del objeto usando una clave, "key", seguido por dos puntos y despues su valor, "value". Separamos cada atributo por una coma.
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32
} // El ultimo valor ya no necesita coma. Todo lo que este dentro de las comillas ocupara un lugar en la memoria ram como una sola variable.

var axel = {
  nombre: 'Axel',
  apellido: 'Godinez', // Cada atributo consiste de un nombre y un valor. De alguna manera es una variable dentro de una variable, como muñecas rusas.
  edad: 34
}

var jorge = {
  nombre: 'Jorge',
  apellido: 'Medina',
  edad: 35
}
// Aprendimos a declarar funciones usando una variable como parametro:
// function imprimirNombreEnMayusculas(nombre) {
//   nombre = nombre.toUpperCase()
//   console.log(nombre);}
// Donde obtenemos el valor simplemente nombrando a la variable.

// Para utilizar alguno de los valores de un objeto hay diferentes metodos:
// -Usando una variable local
function imprimirNombreEnMayusculasVarLocal(persona) { //Estableciendo un parametro para recibir el objeto
  nombre = persona.nombre.toUpperCase() // Declarando una variable cuyo valor apunta a un atributo del objeto, la sintaxis es sencilla, basta con poner un punto despues del nombre del objeto y seguido por la clave del atributo deseado.
  console.log(nombre);
}

imprimirNombreEnMayusculasVarLocal(jorge)


// - Usandolo implicito dentro de nuestra funcion
function imprimirApellidoEnMayusculasVarLocalImplicita(persona) {
    // Y en lugar de declarar una variable, llamamos al atributo con un punto, y despues la funcion toUpperCase
  console.log(persona.apellido.toUpperCase() );
}

imprimirApellidoEnMayusculasVarLocalImplicita(axel)


//- declarar el parametro como un atributo desglosado.
function imprimiraApellidoEnMayusculasDesglosado({ apellido }) { //solicitamos el atributo que necesitamos desde el parametro de la funcion
  console.log(apellido.toUpperCase());
}

imprimiraApellidoEnMayusculasDesglosado(luis) //lo unico necesario es nombrar el objeto, y el parametro buscara la key deseada por como fue expresada la funcion, ({ nombre }) en este caso
imprimiraApellidoEnMayusculasDesglosado({ apellido: 'Reyes'}) //Es posible establecer un atributo sin haber creado un objeto previamente. Basta que en el parametro "declaremos" el atributo usando llaves.
