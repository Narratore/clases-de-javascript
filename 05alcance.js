//Las variables pueden tener un alcance local (dentro de una funcion) o global (dentro del codigo entero del JS)

var nombre = 'Luis' // Al declararla ya es una variable global, a menos que sea escrita dentro del cuerpo de una funcion.
console.log(nombre);

// al definirla dentro del cuerpo de alguna funcion es una variable local.
function imprimirNombreEnMayusculas(nombre) {
  nombre = nombre.toUpperCase()
  console.log(nombre);
}

imprimirNombreEnMayusculas(nombre)
//cuando llamamos la variable window.nombre estamos solicitando la variable global
// ahora bien, podemos seguir solicitando a la variable global usando su nombre completo (window.nombre)

var apellido = 'Carrillo'
//Si dentro de la funcion llamamos a una variable global, como en:
function imprimirApellidoEnMayusculas() {
  apellido = apellido.toUpperCase()
  console.log(apellido);
}
imprimirApellidoEnMayusculas(apellido)

console.log(`Se modifico la variable global "apellido" ${apellido}`)
//el resultado modificara a window.apellido (global)
//Un side-effect, indeseable y poco profesional
//En cambio, si la usamos como un parametro de la funcion:
// function imprimirApelloidoEnMayusculas(apellido)
// solo se modificara de manera local es decir, dentro de la funcion aunque comparta el nombre que su contraparte global
