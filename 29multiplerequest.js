//En esta clase accederemos a múltiples datos al mismo tiempo. Continuaremos trabajando con los jQuery y la SWAPI.


const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id'

 // Incluyendo todos los datos del request en el cuerpo de la funcion:
function obtenerUnPersonaje(id) { // Establecemos un parametro para poder cambiar el "personaje" de cada request.
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}` // usamos el parametro para modificar la url mediante "replace".
  const opts= { crossDomain:true }
  const onPeopleResponse= function(character) {
    console.log(`Hola, yo soy ${character.name}`);
}

  $.get(url, opts, onPeopleResponse)
}

// nota que funciona con cualquier id. ¿En que orden apareceran los elementos?
obtenerUnPersonaje(1)
obtenerUnPersonaje(2)
obtenerUnPersonaje(3)
//bajo este metodo es imposible saberlo, ya que depende de muchos factores, como el estado del servidor, lo cual esta fuera de nuestro control, y la salida sera aleatoria.
