// Una ultima estructura de control se llama switch. Switch se utiliza para realizar diferentes acciones basadas en múltiples condiciones. Para ejemplificarla hagamos un programa que pregunta tu signo y te devuelve un horóscopo.

var signo = prompt('¿Cual es tu signo?') //Prompt muestra un cuadro de mensaje el cual que le solicita al usuario ingresar algun dato.

// Utilizando las estructuras que ya conocemos, podriamos escribir muchos if para cada signo, pero con switch escribiriemos un codigo mas legible, convirtiendolos en casos.
switch (signo) { // Por parametro utilizaremos la variable que captura el prompt
  case 'aries': // Esto es igual a decir "en caso de que la variable sea aries, ejecuta lo siguiente"
    console.log('Las personas nacidas bajo este signo son mentalmente torpes por naturaleza y aprenden incluso las cosas más simples con suma dificultad. Para lograr que absorban algo de conocimientos es necesario emplear muchas palizas. Por culpa de su idiotez, los de Aries suelen desorganizar todo en su trabajo, por lo cual rápidamente ascienden a los puestos más altos. En los contactos interpersonales, por lo general son más que pesados y muy a menudo sin ningún motivo causan peleas y burdas. Por suerte, no suelen ser longevos.');
  break //Break, sirve para que el browser se salte un bucle.
  case 'tauro':
    console.log('Sus rasgos característicos son: el encanto digno de un Casanova campestre, e inclinaciones hacia la homosexualidad. Vive convencido de ser ombligo del mundo y también por ello, le gusta posar como loco, tanto delante del espejo, como delante de los compañeros de trabajo. Aterroriza a sus familiares más cercanos desde el día de su nacimiento, hasta los últimos momentos de su vejez. Nunca en su vida había leído nada, pero tiene una fuerte opinión acerca de todo. Suda como un cerdo.');
  break
  case 'geminis':
  case 'géminis': //si para el mismo caso hay mas de una forma de solicitud, erstas se enumeran antes de el codigo. (en este ejemplo hay distintas formas en que se escribe el mismo signo zodiacal.)
    console.log('Los nacidos bajo este signo nunca llegan a madurar, ya sea en cuestión de sentimientos, ya sea hablando del intelecto. Lo único que se les da bien es rellenar, una vez por semana, un cupón de la lotería. Aunque intentan ocultarlo, el placer más grande supone para ellos la acción de hurgarse la nariz. Si piensas en invitar a alguien de Géminis a tu casa, tienes que ser consciente de que tienden a robar. Por ello, no te olvides de revisarlos.');
  break
  case 'cancer':
  case 'cáncer':
    console.log('Aquí sobran las palabras... En este caso ni siquiera un manicomio es una opción de ayuda. Los de Cáncer estafan, engañan y les encanta poner la zancadilla a cualquiera, en todo momento. Nunca se puede confiar en ellos (por ejemplo, cuando te dicen que valoran mucho tu amistad, puedes estar más que seguro de que acaban de denunciarte ante el jefe). Todo el tiempo se dejan llevar por la envidia, y después de cumplir los 25 empiezan a perder el pelo y los dientes. Y bien merecido lo tienen.');
  break
  case 'leo':
    console.log('Los de Leo desde el principio de sus vidas suelen sentirse atraídos por las drogas, el alcohol y la depravación de las más brutales. No les gusta estudiar, a duras penas terminan las escuelas (incluso las especiales), y aunque son unos perjuros, con muchas ganas declaran durante pleitos y juicios. Una vez desenmascarados, provocan escándalos y montan escenas. Gracias a Dios, no tienen propia vida familiar.');
  break
  case 'virgo':
    console.log('Las relaciones íntimas con las personas nacidas bajo el signo de Virgo son desastrosas, ya que tanto los hombres como las mujeres en la cama demuestran un encanto digno de un elefante y una creatividad digna de un conejo. No importa el signo del otro de sus padres, los hijos de los de Virgo por lo general terminan en las clínicas para los enfermos mentales. Mentalmente, siempre permanecen siendo vírgenes al cien por cien.');
  break
  case 'libra':
    console.log('Hay que tener muy mala suerte para nacer bajo el signo de Libra. De hecho, esto lo determina todo. Los de Libra tienen una mala memoria y una pésima vista, son manazas por excelencia y su supuesta sagacidad deja mucho que desear. Sus ambiciones profesionales prácticamente no existen, al igual que su sentido del humor. Lo que no tienen es: la suerte en los negocios, la suerte en el amor, el sentido común y la salud. Teniendo en cuenta que, de hecho, disponen de todo lo que no tienen, los de Libra son unos cónyuges perfectos.');
  break
  case 'escorpion':
  case 'escorpio':
  case 'escorpión':
    console.log('Tiene manía persecutoria. Nada es capaz de cambiar su convicción de que por numerosos y constantes fracasos del Escorpio siempre responden otras personas, y nunca él mismo. Ya que vive asustado, suele atacar primero y desde atrás. Con placer tortura los animales y en secreto sueña con el puesto de un vigilante en un zoológico.');
  break
  case 'sagitario':
    console.log('La persona nacida bajo este signo demuestra una gran energía y creatividad: es un activista social nato. Por supuesto, no importa qué se propongan, siempre lo terminan arruinando. En la infancia, robaba caramelos a los niños menores. Tiene un gusto especial por la masturbación y el voyeurismo en baños públicos. De viejo, escribe memorias inventadas e principio al fin.');
  break
  case 'capricornio':
    console.log('Cualquier sociedad sana a la que le importa su progreso debería aislar a los de Capricornio de inmediato. Siendo alcohólicos y analfabetos, con gusto depravan a los jóvenes y abusan de las ancianas hasta quedar impotentes. Sólo sirven para cavar fosas (¡y bajo supervisión!). Su vida familiar y social se puede describir con una cifra, “0”.');
  break
  case 'acuario':
    console.log('Tiene una admirable afición a la gastronomía de pésima categoría y nunca devuelve dinero prestado. Tarde o temprano, los hombres de Acuario resultan ser exhibicionistas, y las mujeres, ninfómanas. Por alguna razón muy extraña, todos ellos se sienten genial cuando internados en psiquiátricos o penitenciarias.');
  break
  case 'piscis':
    console.log('Todo el tiempo provocan peleas y situaciones insanas en casa y en el trabajo. Tienen una mentalidad más básica que el uso del váter. Se les da bien trabajar de controladores de los billetes de autobús, y les gusta la pornografía. Se ruega no dejar que se acerquen a los utensilios más complicados que una maja de patatas, ya que lo estropean todo. Signo de zodiaco adecuado para ellos: ninguno.');
  break
  default: // default se utilizara si la variable no coincide con ninguno de los casos, como un comodin.
  console.log('Eso no es un signo');
}
