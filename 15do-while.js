// Otra forma de enfocar un loop, es condicionarlo a que se ejecute despues de que se realize al menos una vez un comando, y siga haciendolo mientras una condicion sea verdadera.
var contador = 0;
var llueve = () => Math.random()>0.25; //usamos una simulacion de las posibilidades que llueva.

do{
  contador++; // Lo que se encuentre en el cuerpo de "do" correra al menos una vez, despues consultara el argumento de while, si se cumple volvera a ejecutarse "do", y asi hasta que el argumento sea false.
}while(!llueve()); // Esta linea es la negacion (!) de que este lloviendo. En caso de que llueve sea falso, vuelva a ejecutarse el codigo dentro de "do".

var unaOMas = contador===1 ? "vez":"veces"; //una condicion ternaria para usar singular o plural dentro de nuestro mensaje
console.log(`Fui ${contador} ${unaOMas} a ver si llovía`);
