// Reto Crea una funcion que compruebe que una persona es mayor de edad

var axel = {
  nombre: 'Axel',
  edad: 34,
}

var alexis = {
  nombre: 'Alexis',
  edad: 7,
} // Creamos dos objetos que representan personas ficticias, para un caso de mayoria de edad y otro para un menor de edad.

function imprimirSiEsMayorDeEdad(persona) {
  if (persona.edad >= 18 ) { // Llamamos al parametro edad del objeto y lo comparamos con un numero, estableciendo una condicion.
    console.log(`Felicidades ${persona.nombre}, puedes entrar al bar`); // Si la condicion se cumple, nos confirma su mayoria de edad.
  } else {
    console.log(`Lo siento ${persona.nombre}, regresa en unos años`);  // En caso contrario, nos indica que es menor de edad.
  }
}

console.log(imprimirSiEsMayorDeEdad(axel));
console.log(imprimirSiEsMayorDeEdad(alexis));
