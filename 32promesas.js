// Es mejor manejar request como una "promesa". Las promesas, son valores que aun no conocemos, y tienen tres estados:
//Pendiente, "pending", en el cual se encuentra toda promesa una vez que se ha llamado, se mantendra latente hasta que haya una respuesta.
//Cumplido, "fulfilled", donde la promesa se resuelve obteniendo el valor, y asi es posible trabajarlo con alguna funcion bajo el metodo .then
// Rechazado, "rejected", en el caso de que el valor esperado no llego. Bajo este error se ejecutara el codigo que escribamos bajo .catch.


const API_URL= 'https://swapi.cso/api/', PEOPLE_URL = 'people/:id'
const opts= { crossDomain:true } // El error en la url es para que veamos como funciona un rejected

function obtenerUnPersonaje(id) {
  return new Promise((resolve, reject) => { // Las promesas reciben como argumento una funcion con dos parametros (que deberan ser funciones)
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    $ // Esta es la promesa que haremos: obtener un request.
      .get(url, opts,  data => {  // Entonces nuesto .get sera lo prometido, es decir, el valor que  estaba esperando la promesa
          resolve(data) // "resolve" llevara el objeto a una funcion que se ejecutara "en dado caso"  que la promesa se cumpla (.then).
      })
      .fail( () => reject(id) ) //  .fail sera tomado como el incumplimiento a la promesa, y se ejecutara un plan de rechazo
  })
}

onError = id => console.log(`Sucedió un error al obtener el personaje ${id}`)
 // Usamos "id" como argumento de reject. En este caso se utiliza para completar el log del error.

obtenerUnPersonaje(1) // Usamos la función que contiene la promesa, aqui entra al estado "pending". Despues declaramos que pasara en cada respuesta
    //  En caso de que exista la resolucion a nuestra promesa, estaremos en el estado "fulfilled", en el cual es posible trabajar con el objeto recibido.
  .then(character => { // ".then" se relaciona a "resolve". El objeto que recibio resolve durante el request es a su vez enviado a then como argumento.
    console.log(`El personaje 1 es ${character.name}`) // En resumen, "resolve" y "character" es el mismo objeto.
  })
  .catch(onError) //  Si nuestra promesa no se cumple (rejected), .catch se valdra de los argumentos recibidos por reject.
