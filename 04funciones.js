
// Las funciones son fracciones de código reutilizable. En esta clase aprenderemos a definir e invocar nuestras funciones. Para definir una función utilizaremos la palabra reservada “function”.

var nombre = 'Luis', edad = 32

function imprimirEdad(n, e) { // Delimitamos el cuerpo de la función usando llaves { }. Los parámetros de la función son variables u otras funciones que seran solicitadas, y se declaran numerandolas entre paréntesis separadas por comas)
    console.log(`${n} tiene ${e} años`); //dentro del cuerpo de la funcion, especificaremos que papel tienen los parametros solicitados, aqui al ser valores los ocupamos como strigs dentro del log. Aqui son "n" y "e"
}

imprimirEdad(nombre, edad) // Podemos usar solo variables como parametro
imprimirEdad('Frito', 30) //o valores  (strings , numericos, o booleanos...)
imprimirEdad(edad, nombre) // No importa en que orden los coloquemos, usara las variables (aunque no en el papel que esperamos pensando que no las dimos en el orden correcto.)
imprimirEdad() // o hasta usarla sola, lo cual nos dara el resultado sin los parametros llenos (y nos devolvera undefined, es decir, una negacion, falta de.)

// JavaScript es un lenguaje interpretado, esto quiere decir que intentará ejecutar el código sin importar si los parámetros que le pasemos a la función estén invertidos o incluso incompletos.
