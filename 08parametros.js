var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32
}
// Cuando le pasamos un objeto como parámetro, Javascript puede trabajar de diversas formas


function cumpleanos(persona) { // Al llamar al objeto asi, sera una referencia para la función
  persona.edad += 1 // Y en dado caso de que efectuemos alguna operación a un atributo de la referencia
} // Estos se modifican de manera global. Prueba hacer la funcion cumpleanos a luis
// Si ves los atributos de luis, veras que el valor de edad ha sido modificado.

//o podriamos establecer un parametro que va a recibir un atributo
function cumpleanosvalor(edad) { //para que esta funcion trabaje, se debe de introducir el objeto y el atribut e.g. cumpleanos(luis.edad) en la consola
  edad += 1
} // De esta manera no se modificara el objeto de manera globlal, pero la forma de escribir el parametro se volvera mas larga.

// Hay otra forma de solucionar esto, creando un objeto diferente. Esto se efectua usando el metodo return
function cumpleanosreturn(persona) { //establecemos un parametro que recibira un objeto
  return { // Con return indicamos que devolveremos un objeto, y solo se modificara el valor que "pisemos"
    ...persona, // Se escriben tres puntos para indicar que usaremos todos los atributos que contenga el objeto que recibimos.
    edad: persona.edad + 1 // Se establece una clave (en este caso con el mismo nombre que la que pisaremos) y  en su valor se llama al objeto y su atributo, y se le aplica el cambio necesario para formar el nuevo valor.
  }
} //Ahora habra que crear una variable cuyo valor sea la funcion aplicada en el objeto:
// var luisMasViejo = cumpleanosreturn(luis)
//con esto habremos creado un objeto llamado luisMasViejo, con los mismos atributos de luis, pero con los nuevos valores de los atributos que "pisamos".
