var luis = {
  nombre:'Luis',
  apellido:'Carrillo',
  edad:32,
  altura: 1.62
}
var axel ={
  nombre:'Axel',
  apellido:'Godinez',
  edad:35,
  altura: 1.74
}
var jorge={
  nombre:'Jorge',
  apellido:'Medina',
  edad:36,
  altura: 1.76
}
var gonzalo={
  nombre:'Gonzalo',
  apellido:'Pardo',
  edad:36,
  altura: 1.81
}
var jassiel={
  nombre:'Jassiel',
  apellido:'Reyes',
  edad:35,
  altura: 1.78
}
var greta ={
  nombre:'Greta',
  apellido:'Tena',
  edad:29,
  altura: 1.59,
}

var personas = [luis, jorge, jassiel, gonzalo, axel, greta]

// Usaremos el metodo map para transformar un array, el cual itera sobre los elementos en el orden de inserción y devuelve un array nuevo con los elementos modificados.

// Aunque podriamos expresar la funcion dentro del parametro de map, siempe es mas prolijo definir la funcion individualmente. Supongamos que es necesario convertir las estaturas de metros a centimetros.
const pasarAlturaACms = persona => ({
    ...persona,
    altura: persona.altura * 100
}) // Mediante los parentesis indicamos que deseamos nos devuela "return" un nuevo objeto

// Desglosamos la clave de altura, ya que si usamos la funcion  de la forma:
// const pasarAlturaACms = persona => {
//   persona.altura *= 100
//   return persona
// } Nos modificaria el array original, ten mucha atencion en este aspecto.

var personasCms = personas.map(pasarAlturaACms)
// La funcion de map es la de pasar cada objeto del array a la funcion indicada, es decir, personas[0] se volvera el parametro de la funcion pasarAlturaACms (es decir, "persona"), pisara altura, el return llegara a map y seguira con el siguiente objeto, personas[...] terminando armara un nuevo array con todos los elementos.

console.log(personasCms); 
