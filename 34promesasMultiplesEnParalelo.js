// Tambien se puede ocupar un array para hacer multiples promesas, con lo cual escribiremos mucho menos codigo.

const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id'
const opts= { crossDomain:true }

function obtenerUnPersonaje(id) {
  return new Promise((resolve, reject) => {
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    $
      .get(url, opts,  data => {
          resolve(data)
      })
      .fail( () => reject(id))
  })
}

onError = id => console.log(`Sucedió un error al obtener el personaje ${id}`)

var ids = [1, 2, 3, 4, 5, 6, 7] // Listamos en un array los id que deseamos trabajar
var promesas = ids.map(id => obtenerUnPersonaje(id)) // y usamos el metodo map para darle cada id del array a la funcion con la promesa.

Promise
  .all(promesas) // Al darle por argumento a Promise.all la salida del mapeo, ejecutara todos los elementos listados en el array.
  .then(personajes => console.log(personajes)) // Obtenemos un array de objetos, en base a todas las resoluciones de las promesas.
  .catch(onError)
