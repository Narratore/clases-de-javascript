// Si necesitamos comparar el vaor de una variable, existen algunos metodos, los cuales tienen manejan la tarea de formas diferentes. Al usar el caracter igual despues del nombre de una variable JavaScript cambia el valor de esta, asi que es necesario escribir dos o tres signos de igual.
var x = 4, y = "4"
console.log("Doble igual x==y")
console.log(x==y); //aqui vemos el resultado de la comparacion usando dos signos de igual.
// Esta comparacion convierte los valores a un mismo tipo de dato (string) y despues las compara, es decir, el 4 numero se convierte al string "4"
// Sin embargo, este resultado puede generar conflicto dentro de los programas que hagamos, pues al ser distintos tipos de valores, reaccionan diferente.
// para hacer una comparacion que tambien tome en cuenta el tipo de valor debemos usar el operador triple igual (===)
console.log("Triple Igual x===y");
console.log(x===y);
// El resultado de la triple igualdad es "false", por las razones previamente expuestas. Esta es una buena practica, pues no da a lugar a errores de "tipo de variable",

// Comparacion de objetos

// Teniendo objetos de nombre distinto con atributos iguales:
var luis = {
  nombre: 'Luis'
}

var zero = {
  nombre: 'Luis'
}

// Al comparar con doble o triple igual con  los objetos, el resultado sera siempre "false", pues solo sus atributos son iguales.
console.log("Comparar luis==zero");
console.log(luis==zero);
console.log("luis===zero");
console.log(luis===zero);
// Comparando los atributos de ambos si hay coincidencia
console.log("luis.nombre === zero.nombre");
console.log(luis.nombre === zero.nombre);

// Cuando una variable apunta a un objeto, esta sera igual al objeto al que apunta.

var apodo = luis

console.log("Compara la variable apodo con el objeto luis: apodo == luis");
console.log(apodo===luis);

// Ahora supongamos que queremos cambiar el valor de nombre dentro del objeto luis:
// luis.nombre = 'Alberto'
// Es posible tambien modificar su  atributo desde "apodo", ya que apunta al mismo lugar en la memoria ram, intenta usar el siguiente comando en la consola:
// apodo.nombre = 'Alberto'
// Despues revisa los atributos del objeto luis, y recarga la pagina.

// El siguiente objeto tiene todos los atributos de luis, pero es un objeto nuevo e independiente.
var alias = {
    ...luis
}

console.log("alias===luis");
console.log(alias===luis);
console.log("alias.nombre===luis.nombre");
console.log(alias.nombre===luis.nombre);
