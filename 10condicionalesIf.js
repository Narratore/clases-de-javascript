// Ahora veamos los valores booleanos y su utilidad
var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32,
  ingeniero: false,
  cuentacuentos: true,
  cantante: false,
  bailarin: false,
  estudiante: true,
} //como ves, ahora tenemos parametros que no son ni string ni numericos, denominados booleanos, cuyos valores son "true" o "false" (verdadero o falso).

function imprimirProfesiones(persona) {
  console.log(`${persona.nombre} es:`)
  //ahora veremos la utilidad de los booleanos, usando condicioles:
  if (persona.ingeniero) { // "if" pregunta el valor booleano de el atributo requerido, si el valor es "true" se llevara a cabo lo que se indique en el cuerpo de if.
    console.log('Ingeniero');
} else { // En caso contrario, si el valor es "false", se ejecutara lo que se dicte en el cuerpo de "else"
    console.log('No es ingeniero');
    }
  if (persona.cuentacuentos) {
    console.log('Cuentacuentos');
  } else {
    console.log('No es cuentacuentos');
    }
  if (persona.cantante) {
    console.log('Cantante');
  } else {
    console.log('No es cantante');
    }
  if (persona.bailarin) {
    console.log('Bailarin');
  } else {
    console.log('No es bailarin');
    }
  if (persona.estudiante) {
    console.log('Estudiante');
  } else {
    console.log('No es estudiante');
    }
}
// Al usar las condicionales indicamos que se va a ejecutar en casos distintos.
console.log(imprimirProfesiones(luis));
