// En esta clase aprenderemos variables de tipo números y las operaciones más comunes en este tipo de variables. En esta clase veremos operadores matemáticos.

var edad = 32
edad = edad + 1 // Hay diferentes formas de trabajar operadores matimaticos, la manera acostumbrada de escribir operaciones matematicas y tambien funciona anticipando que se hara  con la variable, asi nos ahorramos escribirla dos veces.
//Para usar esa forma mas sencilla declaramos que variable se trabajara, usamos el operador seguido de "=" y el valor que se usara en dicha operación. e.g. edad += 1

var peso = 69
// peso = peso - 2
peso -= 2 // Mira como funciona tambien de esta manera, hasta aqui nuestra variable tiene un valor de 67 (69-2)

var pesoA = peso // Retomemos ese valor.
var sandwich = 1
pesoA = pesoA + sandwich // Podemos hacer operaciones matematicas entre variables que tengan valor numerico. El resultado debe ser 68 (67+1)

var pesoB = pesoA
var ejercitar = 3
// pesoB = pesoB - ejercitar
pesoB -= ejercitar // Y asi podemos trabajar los valores numericos de las variables para ajustarse al objetivo de nuestro programa. Aqui el valor debe ser 65 (68-3)

var precioDelVino = 200.3

// En operaciones con decimales debemos realizar operaciones adicionales para conseguir un resultado preciso.
var totalDecimales = precioDelVino *3 // Haciendolo asi, hay un error inherente de javaScript, el cual nos devuelve un valor de 600.9000000000001, pues designa cierta cantidad de bytes cuando trabaja con decimales.
var totalPasandoAEnteros = precioDelVino*100*3/100 // Una solucion es pasar nuestro valor a enteros y despues hacer la multiplicacion, al final lo regresamos a decimales.
var totalRedondeado = Math.round(precioDelVino * 3) //Para redondear una operación se utiliza la función: Math.round
var total = Math.round(precioDelVino*100*3) /100 // Si no sabemos cuantos decimales van a trabajarse, podemos combinar los dos metodos, primero redondear y despues dividir
var totalStr = total.toFixed(3) // la funcion toFixed  sirve para especificar cuantas posiciones despues del punto son permitidas. y el resultado lo arroja en forma de string. Al usar 3 el resultado seran hasta las milesimas (600.900) Y ese valor sera una cadena de texto.
var total2 = parseFloat(totalStr) // "parse" significa transformar un valor de una identidad a otra (en este caso, de un valor string a un valor numero) "Float" se refiere a que sera un valor con decimales.

var tacos = 12
var personas = 4
var cuantosTacosPorPersona = tacos / personas
