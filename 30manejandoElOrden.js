// Esta es una manera de asignar un orden a los request que ejecutemos
function obtenerUnPersonaje(id, callback) { // Se establece otro parametro.
  const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id'
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}`
  const opts= { crossDomain:true }
  function onPeopleResponse(character){
    console.log(`Hola, yo soy ${character.name}`)
    callback ? callback() : 0 // con esta instruccion decimos: si hay un segundo argumento al memento de llamar a esta función, ejecutalo despues de terminar esta, "encolalo". En lugar de callback podemos usar otro nombre cualquiera, como "cb" o "fn"
  }

  $.get(url, opts, onPeopleResponse)
}

obtenerUnPersonaje(1, function(){ // Ese segundo parametro se convertira en el orden en que deben ejecutarse las solicitudes.
  obtenerUnPersonaje(2, function(){ //cada solicitud añadida va dentro del cuerpo de la anterior.
    obtenerUnPersonaje(3, function(){
      obtenerUnPersonaje(4, function(){
        obtenerUnPersonaje(5, function(){
          obtenerUnPersonaje(6, function(){
            obtenerUnPersonaje(7)
          })
        })
      })
    })
  })
})
// mira como se forma una piramide, mejor conocida como"piramid of doom" o "callback hell"
