var luis = {
  nombre:'Luis',
  apellido:'Carrillo',
  edad:32,
  altura: 1.62
}
var axel ={
  nombre:'Axel',
  apellido:'Godinez',
  edad:35,
  altura: 1.74
}
var jorge={
  nombre:'Jorge',
  apellido:'Medina',
  edad:36,
  altura: 1.76
}
var gonzalo={
  nombre:'Gonzalo',
  apellido:'Pardo',
  edad:36,
  altura: 1.81
}
var jassiel={
  nombre:'Jassiel',
  apellido:'Reyes',
  edad:35,
  altura: 1.78
}
var greta ={
  nombre:'Greta',
  apellido:'Tena',
  edad:29,
  altura: 1.59,
}

var personas = [luis, jorge, axel, gonzalo, jassiel, greta]

const ESTATURA_PROMEDIO = 1.75
const esAlta = ({altura}) => altura > ESTATURA_PROMEDIO
const esBajita = ({altura}) => altura < ESTATURA_PROMEDIO

// Ahora probaremos la funcion filter, que nos ayuda a seleccionar arrays de acuerdo a algun criterio

// Creamos una variable que consista en una funcion filter aplicada al array que deseamos filtrar.
var personasAltas = personas.filter(esAlta) // Le damos por parametro una funcion que aplique un criterio (o podriamos escribir aqui la logica, pero seria mas dificil de leer)

var personasBajitas = personas.filter(esBajita)

console.log(personasAltas); // el resultado de filter es un nuevo array que incluye solo a los elementos que cumplen el criterio.
console.log(personasBajitas);
