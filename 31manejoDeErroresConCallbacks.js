//Para solucionar el problema de quedarnos sin conexión u otro error en medio de una sucesión de callbacks, utilizamos el método fail().

function obtenerUnPersonaje(id, callback) {
  const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id'
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}`
  const opts= { crossDomain:true }
  const error= ()=>{console.log(`ERROR: No se pudo obtener el personaje ${id}`)}
  $ //el metodo fail llamara a una funcion en caso de que no se obtenga el request.
    .get(url, opts, callback)
    .fail(error)
}

// Escribimos la funcion para el callback directamente en el argumento para que en caso del error, pare y ejecute el ".fail"
obtenerUnPersonaje(1, function(character){
  console.log(`Hola, yo soy ${character.name}`)
  obtenerUnPersonaje(2, function(character){  //bajo este esquema es necesario expresar el callback nuevamente para cada request.
    console.log(`Hola, yo soy ${character.name}`)
    obtenerUnPersonaje(3, function(character){
      console.log(`Hola, yo soy ${character.name}`)
      obtenerUnPersonaje(4, function(character){
        console.log(`Hola, yo soy ${character.name}`)
        obtenerUnPersonaje(5, function(character){
          console.log(`Hola, yo soy ${character.name}`)
          obtenerUnPersonaje(6, function(character){
            console.log(`Hola, yo soy ${character.name}`)
            obtenerUnPersonaje(7, function(character){
              console.log(`Hola, yo soy ${character.name}`)
            })
          })
        })
      })
    })
  })
}) // Sigue presentandose el "callback hell", y luce aun mas enredado con las previsiones de error.
