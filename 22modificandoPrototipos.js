function Persona(nombre, apellido, altura) {
  this.nombre = nombre
  this.apellido = apellido
  this.altura = altura
  this.nacionalidad = 'Mexicano'
}

const ESTATURA_PROMEDIO = 1.75

// Recuerda escribir las funciones que vayas a darle a tu prototipo antes de crear objetos con el, en caso contrario no seran incluidas en los objetos.

Persona.prototype.saludar = () => {
  console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
}

Persona.prototype.soyAlto = () => {
  this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
}

var luis = new Persona('Luis', 'Carrillo', 1.62)
var greta = new Persona('Greta', 'Tena', 1.59)
var axel = new Persona('Axel', 'Godinez', 1.76)

console.log("axel.soyAlto()");
console.log(axel.soyAlto());
// Al llamar a la funcion soyAlto con "axel" ahora que esta escrita como una arrow function tambien devuelve el "else". Este efecto se explicara en la siguiente clase.
