const API_URL= 'https://swapi.co/api/', PEOPLE_URL = 'people/:id'
const opts= { crossDomain:true }

function obtenerUnPersonaje(id) {
  return new Promise((resolve, reject) => {
  const url= `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    $
      .get(url, opts,  data => {
          resolve(data)
      })
      .fail( () => reject(id) )
  })
}

onError = id => console.log(`Sucedió un error al obtener el personaje ${id}`)

obtenerUnPersonaje(1)
  .then(character => {
    console.log(`El personaje 1 es ${character.name}`)
    return obtenerUnPersonaje(2) // Antes de cerrar el cumplimiento, encadenamos la funcion para el siguiente personaje.
  })
  //  El siguiente resolve es independiente del primero.
  .then(character2 => { // Al ser una resolucion nueva, podremos nombrar al objeto de otro modo
    console.log(`El personaje 2 es ${character2.name}`)
    return obtenerUnPersonaje(3)
  })

  .then(character => { // o usar el mismo nombre sin que cause conflictos.
    console.log(`El personaje 3 es ${character.name}`)
    return obtenerUnPersonaje(4)
  })

  .then(character => {
    console.log(`El personaje 4 es ${character.name}`)
    return obtenerUnPersonaje(5)
  })

  .then(character => {
    console.log(`El personaje 5 es ${character.name}`)
    return obtenerUnPersonaje(6)
  })

  .then(character => {
    console.log(`El personaje 6 es ${character.name}`)
    return obtenerUnPersonaje(7)
  })

  .then(character => {
    console.log(`El personaje 7 es ${character.name}`)
  }) // Aqui ya no encadenamos nada, y termina nuestro programa.
  .catch(onError) // No es necesario darle un catch a cada eslabon de promesas, un catch sirve para todos.
