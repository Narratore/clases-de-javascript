// En JavaScript, los parámetros de funciones son por defecto undefined. De todos modos, en algunas situaciones puede ser útil colocar un valor definido para que lo evalúe como verdadero.
//Recuerda que en terminos de boleanos, 0, NaN, undefined, un string '' vacio, null y false son equivalentes. En Cambio cualquier valor diferente a ellos se evaluara como true (aun objetos {} o arrays [] vacios son tomados como true).

const ESTATURA_PROMEDIO = 1.75

class Persona {
 constructor(nombre, apellido, altura) {
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
    this.nacionalidad = 'Mexicano'
  }
  saludar(fn) { // Aunque esta propiedad de Persona funciona sin argumentos, si establecemos un parametro estamos anticipando como se comportara esta funcion en caso de recibir algun parametro (en este ejemplo le daremos una funcion)
    var { nombre, apellido } = this // Al desestructurar previamente dentro del cuerpo, haremos que sea posible mandar estos parametros a alguna funcion que vaya a interactuar con esta.
    console.log(`Hola, soy ${nombre} ${apellido}`) //
    if (fn) fn(nombre, apellido, false) // Esto podriamos leerlo: si se recibe una funcion como parametro, mandare los siguientes argumentos a aquella.
  }
  soyAlto(){
    this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
  }
}

class Desarrollador extends Persona {
  constructor(nombre, apellido, altura) {
    super(nombre, apellido, altura)
  }
  saludar(fn) {
    var { nombre, apellido } = this
    console.log(`Hola, soy ${nombre} ${apellido} y soy desarrollador/a.`)
    fn ? fn(nombre, apellido, true) : 0 // Al expresar de forma ternaria esta condicion, debemos dar forzosamente un "else".
  }
}

function responderSaludo(nombre, apellido, esDev,) { // Esta funcion va a esperar ciertos parametros para tener diferentes comportamientos. Al crear la condicion de la funcion vacia (fn) en "saludar", estamos parametrizando (es decir, cuales seran los valores de entrada para esta funcion.)
  console.log(`Buen dia ${nombre} ${apellido}`); // Este primer log aparecera sin niguna condicion cuando se de responderSaludo por parametro.
  esDev ? console.log(`Ah mira, no sabia que eres desarrollador`) : 0 // Solo los objetos con el prototipo Desarrollador cumpliran con esta condicion, pues marcamos "true" como tercer argumento.
}

var luis = new Desarrollador('Luis', 'Carrillo', 1.62)
var greta = new Persona('Greta', 'Tena', 1.59)
var axel = new Desarrollador('Axel', 'Godinez', 1.76)
console.log("luis.saludar()");
luis.saludar()
console.log("greta.saludar(responderSaludo)");
greta.saludar(responderSaludo) // Mira como reacciona esta funcion al recibir otra funcion como parametro.
console.log("axel.saludar(responderSaludo)");
axel.saludar(responderSaludo)
// Cuando mandas una función como parámetro no se usan los paréntesis, solo necesitas el nombre de la función que quieres ejecutar. Ahora, dentro del codigo de esa función, si debes utilizar los paréntesis para "llamar" a la función.
