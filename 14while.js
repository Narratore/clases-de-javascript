var luis = {
  nombre: 'Luis',
  apellido: 'Carrillo',
  edad: 32,
  peso: 69,
}
// La estructura while al igual que for es repetitiva, aunque su uso es algo mas avanzado.
// Siguiendo el ejemplo anterior, trabajemos una simulacion del cambio de peso de una persona, pero ahora con una optica diferente: en esta ocasion tendremos una meta del peso ideal de la persona, y buscaremos cuantos dias le lleva alcanzarla.
const CAMBIO_DE_PESO = 0.3

const aumentarDePeso = (persona, incremento) => persona.peso += CAMBIO_DE_PESO
const adelgazar = (persona, incremento) => persona.peso -= CAMBIO_DE_PESO

const comeMucho = () => Math.random() < 0.3 //usamos random para suponer que la probabilidad de que luis ingiera muchas calorias es del 30%
const haceEjercicio = () => Math.random() < 0.4 // y que se ejercite sea del 40%

const META = luis.peso - 3 // Establecemos la "meta"
var dias = 0 //creamos una variable que se aumentara por cada iteracion de nuestro programa.

while (luis.peso > META) { // "while" se ejecutara ciclicamente hasta que este argumento resulte en "false".
  if (comeMucho()) {
    aumentarDePeso(luis)
  }
  if (haceEjercicio()) {
    adelgazar(luis)
} // usamos dos "if" pues puede ser el caso que el mismo dia luis decida hacer ejercicio a pesar de que comio mucho.
  dias ++ // al final de cada escenario transcurre un dia mas.
}

// Cada vez que corra el programa habra un resultado diferente gracias al uso de random. Compruebalo refrescando la pagina.
console.log(`Pasaron ${dias} dias hasta que ${luis.nombre} bajo 3kg`);
