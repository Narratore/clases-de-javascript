//Las clases de JavaScript son introducidas en el ECMAScript 2015 y son una mejora en la sintaxis sobre la herencia basada en prototipos.
const ESTATURA_PROMEDIO = 1.75

class Persona { // La sintaxis es mas limpia usando class.
 constructor(nombre, apellido, altura) { // El método constructor es un método especial para crear e inicializar un objeto creado a partir de una clase.
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
    this.nacionalidad = 'Mexicano'
  }
  saludar() { // Las funciones atributo se declaran dentro de nuestro prototipo (recuerda, sigue siendo un prototipo declarado bajo otro metodo).
    console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
  }
  soyAlto(){
    this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
  }
}

class Desarrollador extends Persona { // "extends" se usa en declaraciones de clases que seran "hijas" de otra.
  constructor(nombre, apellido, altura) {
    super(nombre, apellido, altura)
  }
  saludar() { // Las funciones atributo que se llamen igual que en la clase padre se pisaran al usarlas en la clase hija.
    console.log(`Hola, soy ${this.nombre} ${this.apellido} y soy desarrollador/a.`)
  }
}

// Ahora crea objetos con ambos prototipos (Persona y Desarrollador) y revisa sus atributos y prueba funciones para ambos prototipos.
// var luis = new Persona('Luis', 'Carrillo', 1.62)
// var greta = new Persona('Greta', 'Tena', 1.59)
// var axel = new Desarrollador('Axel', 'Godinez', 1.76)
