// Los objetos en JavaScript son “contenedores” dinámicos de propiedades. Estos objetos poseen un enlace a un objeto prototipo. Cuando intentamos acceder a alguna propiedad del objeto, esta no sólo es buscada en el propio objeto, sino también en el prototipo del objeto, y si ese prototipo es "heredado" de algun otro, tambien lo busca en el prototipo "padre", así sucesivamente hasta el prototipo de "object".

const ESTATURA_PROMEDIO = 1.75

// Establecemos una funcion que se encargara de añadir las propiedades de un prototipo a otro.
function heredaDe(prototipoHijo, prototipoPadre) {
  var fn = function() {} // Esta es una funcion vacia, solo para manejar los atributos del prototipo de manera local.
  fn.prototype = prototipoPadre.prototype // Gracias a esto es que no pisamos los atributos de Persona, por que trabajeramos sobre la funcion vacia.
  prototipoHijo.prototype = new fn // Hara un nuevo objeto prototipo con la herencia del prototipo padre.
  prototipoHijo.prototype.constructor = prototipoHijo; // Mandara todas las propiedades añadidas por herencia al primer parametro.
}

// Definimos un prototipo "padre", el cual guardara ciertos atributos/propiedades que puede heredar a otros.
function Persona(nombre, apellido, altura) {
  this.nombre = nombre
  this.apellido = apellido
  this.altura = altura
  this.nacionalidad = 'Mexicano'
}

// Añadimos una funciones a las propiedades del prototipo padre.
Persona.prototype.saludar = function () {
  console.log(`Hola, mi nombre es ${this.nombre} ${this.apellido}`)
}
Persona.prototype.soyAlto = function () {
  this.altura > ESTATURA_PROMEDIO ? console.log(`Soy mas alto que el promedio`) : console.log(`Soy chaparrito`)
}

 //Creamos un nuevo protitipo, que sea para desarrolladores.
function Desarrollador(nombre, apellido) {
  this.nombre = nombre
  this.apellido = apellido
}

heredaDe(Desarrollador, Persona) // Asi "Desarrollador" heredara los atributos de "Persona", es importante donde esta colocada;

// Ya que el prototipo padre tambien tiene una funcion saludar, la cual deseamos modificar para el prototipo hijo, es necesario declarar el nuevo saludo despues de la "herencia".
Desarrollador.prototype.saludar = function () {
  console.log(`Hola, soy ${this.nombre} ${this.apellido} y soy desarrollador/a.`)
}

// Prueba a crear objetos con el prototipo Desarrollador.
// var luis = new Persona('Luis', 'Carrillo', 1.62)
// var greta = new Persona('Greta', 'Tena', 1.59)
// var axel = new Persona('Axel', 'Godinez', 1.76)

// La herencia como tal no existe en JS, pero si existen herencias de prototipo. El metodo usado aqui era la unica forma de conseguirla, pero en la siguiente clase veremos como han solucionado esto de una forma mas sencilla en las nuevas versiones.
