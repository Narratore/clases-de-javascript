//Una tradicion muy "saludable" es que nuestra primera interaccion usando un nuevo lenguaje, sea saludar a todos:
console.log('Hola mundo JS!');

// Estaremos interactuando con las variables a traves de la consola de tu navegador. Para esablecer una variable, usamos "var" seguido del nombre con que queramos definirla, en este caso su nombre es "nombre".
var nombre; //podemos terminar nuestras lineas con punto y coma, pero en JS no es obligatorio su uso salvo en algunos casos muy especificos, la convencion entre la mayoria de los programadores es omitirlo lo mas posible.
// Y a esta varible, le podemos asignar un valor con el signo =
nombre = 'Luis'//el valor debe ser entrecomillas simples '' para identificarlo como un string. por convencion. En el teclado latam es la tecla despues del 0 en la parte superior.

console.log('Hola ' + nombre)

var apellido = 'Carrillo' //en esta ocasion definimos la variable y le damos un valor en string en la misma linea, lo cual es igual a expresarlo
// var apellido
// apellido = 'Carrillo'
console.log('Como esta usted ' + nombre + ' ' + apellido)//usamos un espacio entrecomillado para separar los dos strings

var edad = 32

console.log('Tengo ' + edad + ' años.');//recuerda usar un espacio al final del primer string y uno al comienzo del que va despues de tu variable. Tu variable tambien puede usar espacios y todas las palabras que necesites, mientras no cierres las comillas.
// JS es un lenguaje debilmente tipado, es decir que acepta valores tipo string o en tipo numero indistintamente
