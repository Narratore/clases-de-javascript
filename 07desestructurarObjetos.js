var axel = {
  nombre: 'Axel',
  apellido: 'Godinez',
  edad: 34
}

var jorge = {
  nombre: 'Jorge',
  apellido: 'Medina',
  edad: 35
}

// - Otra forma de usar atributos es desglosando a la variable local
function imprimirApellidoEnMayusculas(persona) { // Estrablecemos un parametro que recibira un objeto.
  var { apellido } = persona // Creamos una variable y por nombre usamos un atributo deglosado, y su valor apunte a la variable local.
  console.log(apellido.toUpperCase()); // y llamamos directamente al atributo dentro de la funcion.
}

imprimirApellidoEnMayusculas(axel)

// Este es el metodo indicado en caso de que necesitemos mas de un atributo. Al llamar al objeto tenemos la posibilidad de ocupar cualquiera de ellos.
function imprimirNombreYEdad (persona) {
  var { nombre, edad } = persona // No es necesario escribir toda la variable para cada atributo, pues al desglosar podemos nombrar a todos los atributos que necesitemos del objeto en una sola linea.
  console.log(`Hola, me llamo ${nombre} y tengo ${edad} años`); // de la forma que mas te guste usar.
}

imprimirNombreYEdad(axel)
imprimirNombreYEdad(jorge)
